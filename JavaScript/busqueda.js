let resultado = JSON.parse(localStorage.getItem('resultado'));
cargar();

function cargar() {
    if (resultado !== null && resultado !== undefined) {
        for (let i = 0; i < resultado.length; i++) {
            const element = resultado[i];
            let div = document.createElement('div');
            let contenedor = document.getElementById('padre');
            div.classList = 'columns small-12 medium-6 large-4 item';
            div.innerHTML = '<img src="' + element.img + '"alt="' + element.nombre + '"class="item-img">'
                + '<h3 class="separar2">' + element.nombre + '</h3>'
                + '<h5 class="separar2">' + element.unombre + '</h5>'
                + '<button class="separar" type="button" onclick="ver(' + element.id + ')">Ver Detalles</button>';
            contenedor.appendChild(div);
        }
    }else {
        let contenedor = document.getElementById('padre');
        let msj = document.createElement('div');
        msj.innerHTML = '<p>No hay elementos que mostrar</p>';
        contenedor.appendChild(msj);
    }
}
function ver(producto) {
    let auxi = buscarproducto(producto);
    localStorage.removeItem('viendo');
    localStorage.setItem('viendo', JSON.stringify(auxi));
    window.location.href = 'pagina-producto.html'
}
function buscarproducto(producto) {
    let bd = JSON.parse(localStorage.getItem('datebase'));
    for (let i = 0; i < bd.length; i++) {
        const element = bd[i];
        if (element.id == producto) {
            return element;
        }
    }
}