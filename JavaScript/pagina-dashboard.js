
let bd = JSON.parse(localStorage.getItem('datebase'));
let miscambalaches = [];
let actual = JSON.parse(sessionStorage.getItem('usuario'));

bienvenida();
load();

function bienvenida() {
    if (actual !== null) {
        let bn = document.getElementById('bienvenida');
        let ms = document.createElement('p');
        ms.innerHTML = 'Bienvenido <b>' + actual.nombre + '</b>, aqui podrá encontrar el estado actual de sus Cambalaches y sus productos registrados.'
        bn.appendChild(ms);
    } else {
        window.location.href = 'pagina-login.html';
    }
}

function load() {
    if (bd !== null && bd !== undefined) {
        for (let i = 0; i < bd.length; i++) {
            const element = bd[i];
            if (element.usuario == actual.id) {
                miscambalaches.push(bd[i]);
            }
        }
        if (miscambalaches.length > 0) {
            for (let i = 0; i < miscambalaches.length; i++) {
                const element = miscambalaches[i];
                let div = document.createElement('div');
                let contain = document.getElementById('container');
                div.classList = 'columns small-12 medium-6 large-4 datos';
                div.innerHTML = '<img src="' + element.img + '" alt="' + element.nombre + '"class="item-img">'
                    + '<label>' + element.nombre + '</label> <br>'
                    + '<button type="button" class="btn-ver" onclick="ver(' + element.id + ')">Ver</button> <br>'
                    + '<button onclick="editar(' + element.id + ')" type="button" class="btn-editar">Editar</button> <br>'
                    + '<button onclick="eliminar(' + element.id + ')" type="button" class="btn-eliminar">Eliminar</button>';
                contain.appendChild(div);
            }
        } else {
            let div = document.createElement('div');
            let contain = document.getElementById('container');
            div.classList = 'columns small-12 medium-12 large-12';
            div.innerHTML = '<p>' + actual.nombre + ', no tienes productos agregados.</p>';
            contain.appendChild(div);
        }
    } else {
        let div = document.createElement('div');
        let contain = document.getElementById('container');
        div.classList = 'columns small-12 medium-12 large-12';
        div.innerHTML = '<p>' + actual.nombre + ', no tienes productos agregados.</p>';
        contain.appendChild(div);
    }
}

function eliminar(producto) {
    let acepto = window.confirm('¿Esta seguro de eliminar este producto?');

    if (acepto) {
        for (let i = 0; i < bd.length; i++) {
            if (bd[i].id == producto) {
                bd.splice(i, 1);
            }
        }
        localStorage.removeItem('datebase');
        localStorage.setItem('datebase', JSON.stringify(bd));
        location.reload();
        alert('Artículo eliminado correctamente.');
    }
}

function editar(producto) {
    let auxi = buscarproducto(producto);
    localStorage.removeItem('editando');
    localStorage.setItem('editando', JSON.stringify(auxi));
    window.location.href = 'pagina-crear-editar.html';
}

function buscarproducto(producto) {
    for (let i = 0; i < bd.length; i++) {
        const element = bd[i];
        if (element.id == producto) {
            return element;
        }
    }
}

function ver(producto) {
    let auxi = buscarproducto(producto);
    localStorage.removeItem('viendo');
    localStorage.setItem('viendo', JSON.stringify(auxi));
    window.location.href = 'pagina-producto.html'
}

