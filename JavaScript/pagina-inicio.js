let basedatos = JSON.parse(localStorage.getItem('datebase'));
let uno = {};
let dos = {};
let categorias = [['Electrónico', 'https://observatoriodenoticias.redue-alcue.org/wp-content/uploads/2019/10/peru-800x445.jpg'],
['Hogar', 'https://www.infochannel.info/sites/default/files/2019/08/30/portada_1240_app.jpg'],
['Negocio', 'https://www.brainsins.com/es/wp-content/uploads/2015/08/influencia-parametros-negocio-conversion-ecommerce.jpg']];
crearCategorias();
ultimo();
document.getElementById('btnBuscar').addEventListener('click', buscar_producto); 

function buscar_producto() {
    let resultado = [];
    let busqueda = document.getElementById('txtbusqueda').value;
    if (busqueda !== '') {
        if (basedatos !== null && basedatos !== undefined) {
            for (let i = 0; i < basedatos.length; i++) {
                const element = basedatos[i];
                if (element.categoria == busqueda || element.nombre == busqueda || element.nombre.includes(busqueda)) {
                    resultado.push(element);
                }
            }
            localStorage.removeItem('resultado');
            localStorage.setItem('resultado', JSON.stringify(resultado));
            window.location.href = "busqueda.html";
        } else{
            document.getElementById('txtbusqueda').value = 'No se encontro producto.';
        }
    }
    else {
        document.getElementById('txtbusqueda').value = 'Indique su busqueda.';
    }
}

function ver(num) {
    if (num == '1') {
        localStorage.removeItem('viendo');
        localStorage.setItem('viendo', JSON.stringify(uno));
        window.location.href = 'pagina-producto.html'
    } else {
        localStorage.removeItem('viendo');
        localStorage.setItem('viendo', JSON.stringify(dos));
        window.location.href = 'pagina-producto.html'
    }
}

function crearCategorias() {
    if (categorias !== null && categorias !== undefined) {

        for (let i = 0; i < categorias.length; i++) {
            const element = categorias[i];
            let container = document.getElementById('container');
            let aux = document.createElement('div');
            aux.classList = 'columns small-12 medium-6 large-4 item';
            aux.innerHTML = '<img src="' + element[1] + '" alt="' + element[0] + '" class="item-img">'
                + '<button class="tituloarticulo" onclick="buscar(' + i + ')">' + element[0] + '</button>';
            container.appendChild(aux);
        }
    } else {
        let msj = document.createElement('div');
        msj.innerHTML = '<p>No ahi categorias que mostrar.</p>';
        container.appendChild(msj);
    }
}

function buscar(cat) {
    let resultado = [];
    let ubicacion = parseInt(cat, 10);
    let categoria = categorias[ubicacion][0];
    for (let i = 0; i < basedatos.length; i++) {
        const element = basedatos[i];
        if (element.categoria == categoria) {
            resultado.push(element);
        }
    }
    localStorage.removeItem('resultado');
    localStorage.setItem('resultado', JSON.stringify(resultado));
    window.location.href = "busqueda.html";
}

function ultimo() {
    if (basedatos !== null && basedatos !== undefined) {
        let cambalache1 = document.getElementById('cambalache1');
        let cambalache2 = document.getElementById('cambalache2');
        let nombre1 = document.getElementById('nombre1');
        let nombre2 = document.getElementById('nombre2');

        if (basedatos.length >= 2) {
            let p1 = basedatos[basedatos.length - 1];
            let p2 = basedatos[basedatos.length - 2];
            uno = p1;
            dos = p2;

            cambalache1.src = p1.img;
            nombre1.value = p1.nombre;
            cambalache1.alt = p1.nombre;
            cambalache2.src = p2.img;
            nombre2.value = p2.nombre;
            cambalache2.alt = p2.nombre;
        } else {
            let msj = document.getElementById('msj');
            let div = document.createElement('div');
            div.innerHTML = '<p>No hay elementos que mostrar.</p>';
            msj.appendChild(div);
            cambalache1.style.display = 'none';
            cambalache2.style.display = 'none';
            nombre1.style.display = 'none';
            nombre2.style.display = 'none';
        }
    } else {
        let cam = document.getElementById('recientes');
        cam.style = 'display: none;';
        let msj = document.getElementById('msj');
        let div = document.createElement('div');
        div.classList = 'columns small-12 large-12';
        div.style.textAlign = 'center';
        div.innerHTML = '<p>No hay elementos que mostrar.</p>';
        msj.appendChild(div);
    }
}

