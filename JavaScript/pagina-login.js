document.getElementById('ingresar').addEventListener('click', login);
document.getElementById('cancelar').addEventListener('click', function () { window.history.go(-1); });
let bd = JSON.parse(localStorage.getItem('usuarios'));

function login() {
    let correo = document.getElementById('correo').value;
    let contrasena = document.getElementById('contrasena').value;
    let login = true;
    if (correo == '' || contrasena == '') {
        let msj = document.getElementById('mensaje');
        msj.value = 'Campos vacios.';
    } else if (bd !== null && bd !== undefined) {
        for (let i = 0; i < bd.length; i++) {
            const element = bd[i];
            if (correo == element.correo && contrasena == element.contrasena) {
                sessionStorage.removeItem('usuario');
                sessionStorage.setItem('usuario', JSON.stringify(element));
                login = false;
                window.location.href = "pagina-dashboard.html";
                break;
            }
        }
        if (login) {
            let msj = document.getElementById('mensaje');
            msj.value = 'Usuario o contraseña invalido';
        }
    } else {
        let msj = document.getElementById('mensaje');
        msj.value = 'Este Usuario no existe en la base de datos, favor registrese.';
        document.getElementById('correo').value = '';
        document.getElementById('contrasena').value = '';
    }
}
