
let datebase = JSON.parse(localStorage.getItem('datebase'));
let editando = JSON.parse(localStorage.getItem('editando'));
document.getElementById('guardar').addEventListener('click', guardar);
document.getElementById('cancelar').addEventListener('click', function () { window.location.href = 'pagina-dashboard.html' });

let editar = false;
let cambalache = {
    id: 0,
    usuario: 0,
    unombre: '',
    nombre: '',
    img: '',
    busco: '',
    descripcion: '',
    categoria: ''
};
cargardatos();

function cargardatos() {
    if (editando !== null && editando !== undefined) {
        cambalache = editando;
        let nombre = document.getElementById('nombre');
        let descripcion = document.getElementById('descripcion');
        let url = document.getElementById('url');
        let busco = document.getElementById('busco');
        let categoria = document.getElementById('categoria');
        nombre.value = cambalache.nombre;
        descripcion.value = cambalache.descripcion;
        url.value = cambalache.img;
        busco.value = cambalache.busco;
        categoria.value = cambalache.categoria;
        editar = true;
        localStorage.removeItem('editando');
    }
}

function getid() {
    let id = localStorage.getItem('idcambalache');
    if (id !== 'NaN' && id !== null) {
        id = JSON.parse(id);
        id = parseInt(id, 10) + 1;
    } else {
        console.log('Entro');
        let menor = 0;
        if (datebase !== null && datebase !== undefined) {
            for (let i = 0; i < datebase.length; i++) {
                const element = datebase[i];
                let aux = parseInt(element.id, 10);
                if (aux > menor) {
                    menor = aux;
                    id = menor + 1;
                }
            }
        } else {
            id = 1;
        }
    }
    localStorage.removeItem('idcambalache');
    localStorage.setItem('idcambalache', JSON.stringify(id));
    return id;
}

function cargarcambalache() {
    let nombre = document.getElementById('nombre').value;
    let descripcion = document.getElementById('descripcion').value;
    let url = document.getElementById('url').value;
    let busco = document.getElementById('busco').value;
    let categoria = document.getElementById('categoria').value;
    cambalache.nombre = nombre;
    cambalache.descripcion = descripcion;
    cambalache.img = url;
    cambalache.busco = busco;
    cambalache.categoria = categoria;
    let aux = JSON.parse(sessionStorage.getItem('usuario'));
    cambalache.usuario = aux.id;
    cambalache.unombre = aux.nombre + '.' + aux.apellido;
    return cambalache;
}

function guardar() {
    let msj = document.getElementById('mensaje');
    cargarcambalache();
    if (cambalache.nombre !== '' && cambalache.descripcion !== '' && cambalache.url !== '' && cambalache.busco !== '') {
        if (editar) {
            cambalache.id = editando.id;
            for (let i = 0; i < datebase.length; i++) {
                const element = datebase[i];
                if (element.id == editando.id) {
                    datebase.splice(i, 1);
                }
            }
        } else {
            cambalache.id = getid();
        }
    }
    if (cambalache.nombre == '') {
        msj.value = 'Nombre del producto requerido.'
    } else if (cambalache.descripcion == '') {
        msj.value = 'Descripción del producto requerido.'
    } else if (cambalache.url == '') {
        msj.value = 'Imagen del producto requerido.'
    } else if (cambalache.busco == '') {
        msj.value = 'Indicar que busca.'
    } else if (cambalache.categoria == 'Seleccionar categoría') {
        msj.value = 'Categoría del producto requerido.'
    } else {
        if (datebase !== null && datebase !== undefined) {
            datebase.push(cambalache);
            registrar();
        } else {
            datebase = [cambalache];
            registrar();
        }
    }
}
function registrar() {
    localStorage.removeItem('datebase');
    localStorage.removeItem('editando');
    localStorage.setItem('datebase', JSON.stringify(datebase));
    window.location.href = 'pagina-dashboard.html';
}