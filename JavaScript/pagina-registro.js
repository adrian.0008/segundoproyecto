document.getElementById('btnRegistrar').addEventListener('click', registrar);
document.getElementById('btnCancelar').addEventListener('click', function () { window.history.go(-1) });

let usuarios = JSON.parse(localStorage.getItem('usuarios'));


function registrar() {
    let nombre = document.getElementById('txtNombre').value;
    let apellidos = document.getElementById('txtApellidos').value;
    let barrio = document.getElementById('txtBarrio').value;
    let direccion = document.getElementById('txtDireccion').value;
    let pais = document.getElementById('cmbCountry').value;
    let ciudad = document.getElementById('txtCiudad').value;
    let correo = document.getElementById('txtCorreo').value;
    let contrasenna = document.getElementById('txtContrasena').value;
    let mensaje = document.getElementById('mensaje');

    let usuario = {
        id: 0,
        nombre: nombre,
        apellido: apellidos,
        direccion: direccion,
        ubicacion: barrio,
        pais: pais,
        ciudad: ciudad,
        correo: correo,
        contrasena: contrasenna
    };
    if (nombre == '') {
        mensaje.value = 'Nombre requerido.';
    } else if (apellidos == '') {
        mensaje.value = 'Apellidos requeridos.';
    } else if (barrio == '') {
        mensaje.value = 'Barrio requerido.';
    } else if (direccion == '') {
        mensaje.value = 'Dirección requerida.';
    } else if (pais == 'Pais') {
        mensaje.value = 'Pais requerido.';
    } else if (ciudad == '') {
        mensaje.value = 'Ciudad requerida.';
    } else if (correo == '') {
        mensaje.value = 'Correo requerido.';
    } else if (contrasenna == '') {
        mensaje.value = 'Contraseña requerida.';
    } else if (contrasenna.length <= 4) {
        mensaje.value = 'Contraseña muy debíl.';
    } else {
        let id = localStorage.getItem('idusuarios');
        if (id !== 'NaN' && id !== null) {
            id = JSON.parse(id);
            id = parseInt(id, 10) + 1;
        } else {
            if (usuarios !== null && usuarios !== undefined) {
                if (usuarios.length > 0) {
                    for (let i = 0; i < usuarios.length; i++) {
                        const element = usuarios[i];
                        let menor = 0;
                        let aux = parseInt(element.id, 10);
                        if (aux > menor) {
                            menor = aux;
                            id = menor + 1;
                        }
                    }
                } else {
                    id = 1;
                }
            } else {
                id = 1;
            }

            localStorage.removeItem('idcambalache');
            localStorage.setItem('idcambalache', JSON.stringify(id));
            usuario.id = id;

            if (usuarios == null || usuarios == undefined) {
                usuarios = [usuario];
                guardar(usuario);
            }else {
                if (verificarUsuario(usuario.correo)) {
                    usuarios.push(usuario);
                    guardar(usuario);
                } else {
                    mensaje.value = 'Este correo ya existe, favor inicie sesión.';
                }
            }
        }
    }
}

function verificarUsuario(email) {
    let aux = true;
    if (usuarios !== null && usuarios !== undefined) {
        for (let i = 0; i < usuarios.length; i++) {
            const element = usuarios[i];
            if (email == element.correo) {
                aux = false;
                return aux;
            }
        }
    } else {
        aux = true;
    }
    return aux;
}
function guardar(usuario) {
    localStorage.removeItem('usuarios');
    localStorage.setItem('usuarios', JSON.stringify(usuarios));
    sessionStorage.removeItem('usuario');
    sessionStorage.setItem('usuario', JSON.stringify(usuario));
    window.location.href = "pagina-dashboard.html";
}