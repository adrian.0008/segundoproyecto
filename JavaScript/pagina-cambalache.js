
let bd = JSON.parse(localStorage.getItem('datebase'));

cargarDatos();
function cargarDatos() {
    if (bd !== null && bd !== undefined) {
        for (let i = bd.length - 1; i > -1; i--) {
            const element = bd[i];
            let div = document.createElement('div');
            let contenedor = document.getElementById('container');
            div.classList = 'columns small-12 medium-6 large-4 item';
            div.innerHTML = '<img src="' + element.img + '"alt="' + element.nombre + '"class="item-img">'
                + '<h3>' + element.nombre + '</h3>'
                + '<h5>' + element.unombre + '</h5>' 
                + '<button type="button" onclick="detalle(' + element.id + ')">Ver Detalles</button>';
            contenedor.appendChild(div);
        }
    }else {
        let contenedor = document.getElementById('container');
        let msj1 = document.createElement('div');
        msj1.classList = 'columns small-12 large-12';
        msj1.style.textAlign = 'center';
        msj1.innerHTML = '<p>No existen cambalaches para mostrar.</p>';
        contenedor.appendChild(msj1);
    }
}

function detalle(producto) {
    let auxi = {};
    for (let i = 0; i < bd.length; i++) {
        const element = bd[i];
        if (element.id == producto) {
            auxi = element;
        }
    }
    localStorage.removeItem('viendo');
    localStorage.setItem('viendo', JSON.stringify(auxi));
    window.location.href = 'pagina-producto.html'
}

